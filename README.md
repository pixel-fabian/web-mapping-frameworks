# Web mapping frameworks

Testing some web-mapping frameworks

- Inspiration: [CartoHack #18 – Reactive Web Maps](https://visualisierung.dgfk.net/en/events/cartohack/18-reactive/)

## Tools used

- [svelte](https://svelte.dev/)
- [Leaflet](https://leafletjs.com/)
- [OpenLayers](https://openlayers.org/)
- [MapLibre](https://maplibre.org/)
- [mapbox](https://docs.mapbox.com/mapbox-gl-js/api/)
- [deck.gl](https://deck.gl/)
- [D3](https://deck.gl/)

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
