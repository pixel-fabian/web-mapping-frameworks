import { writable, derived } from 'svelte/store';

type BathLocation = {
	Name: string;
	description: string;
	lastMeasurementDate: string;
	temperature: string;
	visibilityDepth: string;
	remarks: string;
	district: string;
	smiley: string;
	bodyOfWater: string;
	onr: string;
	bacteriology: string;
	lavatory: string;
	wasteDisposal: string;
	gastronomy: string;
	lifeguard: string;
	beachCharacter: string;
	picture: string;
	licensee: string;
	licenseeUrl: string;
	parkingArea: string;
	sunbathingArea: string;
	fishingAllowed: string;
	campingAllowed: string;
	playground: string;
	barbecueArea: string;
	aquaticsAllowed: string;
	miscellaneous: string;
	profile: string;
	detailDescription: string;
	rating: string;
	bnr: string;
	coordinates: number[];
};

type Geom = {
	type: string;
	coordinates: number[];
};

type Feature = {
	type: string;
	properties: object;
	coordinates: number[];
};

type FeatureCollection = {
	type: string;
	features: Feature[];
};

/**
 * writable store for loading status
 */
export const loaded = writable(false);

/**
 * writable store for locations
 */
export const locations = writable<BathLocation[]>([]);

/**
 * writable store for currently selected locationID
 */
export const currentLocationID = writable<number | null>(null);

/**
 * writable store for currently selected locationID
 */
export const currentLocation = derived(
	[currentLocationID, locations],
	([$currentLocationID, $locations]) => {
		if (typeof $currentLocationID == 'number' && $locations) {
			return $locations[$currentLocationID];
		} else {
			return null;
		}
	}
);

const fetchData = async function () {
	const response = await fetch('http://localhost:5173/data/bathing-spots.geojson').catch(
		(error) => {
			throw error;
		}
	);
	const data = await response.json();
	locations.set(data);
	console.log(data);

	loaded.set(true);
};

await fetchData();
